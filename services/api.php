<?php
 	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "127.0.0.1";
		const DB_USER = "root";
		const DB_PASSWORD = "";
		const DB = "agenda";

		private $db = NULL;
		private $mysqli = NULL;
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();					// Initiate Database connection
		}
		
		/*
		 *  Connect to Database
		*/
		private function dbConnect(){
			$this->mysqli = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB);
		}
		
		/*
		 * Dynmically call the method based on the query string
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['x'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404); // If the method not exist with in this class "Page not found".
		}
				
		
		
		private function tarefas(){	
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			$query="SELECT idTarefa,titulo,descricao,prioridade FROM tarefa order by prioridade desc";
			$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
			if($r->num_rows > 0){
				$result = array();
				while($row = $r->fetch_assoc()){
					$result[] = $row;
				}
				
				$this->response($this->json($result), 200);
			}
			$this->response('',204);
		}
		private function tarefa(){	
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			
			$idTarefa = (int)$this->_request['idTarefa'];
			if($idTarefa > 0){	
				$query="SELECT * FROM tarefa where idTarefa=$idTarefa";
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				if($r->num_rows > 0) {
					$result = $r->fetch_assoc();	
					$this->response($this->json($result), 200);
				}
			}
			$this->response('',204);
		}

		private function numeros(){	
			if($this->get_request_method() != "GET"){
				$this->response('',406);
			}
			for($i=1;$i<=100;$i++){
				$valor=$i;
				if($i%3==0 && $i%5==0){
					$valor="FizzBuzz";
				}else if($i%3==0){
					$valor="Fizz";
				}else if($i%5==0){
					$valor="Buzz";
				}
				$vls[] = $valor;
				
			}
			$result[]=$vls;
			$this->response($this->json($result), 200);
		}

		private function insertTarefa(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$tarefa = json_decode(file_get_contents("php://input"),true);
			$column_names = array('titulo', 'descricao', 'prioridade');
			$keys = array_keys($tarefa);
			$columns = '';
			$values = '';
			foreach($column_names as $desired_key){
			   if(!in_array($desired_key, $keys)) {
			   		$$desired_key = '';
				}else{
					$$desired_key = $tarefa[$desired_key];
				}
				$columns = $columns.$desired_key.',';
				$values = $values."'".$$desired_key."',";
			}
			$query = "INSERT INTO tarefa(".trim($columns,',').") VALUES(".trim($values,',').")";
			if(!empty($tarefa)){
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Tarefa cadastrada com sucesso.", "data" => $tarefa);
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	//"No Content" status
		}
		private function updateTarefa(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$tarefa = json_decode(file_get_contents("php://input"),true);
			$idTarefa = (int)$tarefa['idTarefa'];
			$column_names = array('titulo', 'descricao', 'prioridade');
			$keys = array_keys($tarefa['tarefa']);
			$columns = '';
			$values = '';
			foreach($column_names as $desired_key){ // Check the customer received. If key does not exist, insert blank into the array.
			   if(!in_array($desired_key, $keys)) {
			   		$$desired_key = '';
				}else{
					$$desired_key = $tarefa['tarefa'][$desired_key];
				}
				$columns = $columns.$desired_key."='".$$desired_key."',";
			}
			$query = "UPDATE tarefa SET ".trim($columns,',')." WHERE idTarefa=$idTarefa";
			error_log($query);
			if(!empty($tarefa)){
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Tarefa ".$idTarefa." Alterada com sucesso.", "data" => $tarefa);
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// "No Content" status
		}
		
		private function deleteTarefa(){
			if($this->get_request_method() != "DELETE"){
				$this->response('',406);
			}
			$idTarefa = (int)$this->_request['idTarefa'];
			if($idTarefa > 0){				
				$query="DELETE FROM tarefa WHERE idTarefa = $idTarefa";
				$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
				$success = array('status' => "Success", "msg" => "Tarefa excluida com sucesso.");
				$this->response($this->json($success),200);
			}else
				$this->response('',204);	// If no records "No Content" status
		}
		
		/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();
?>