var app = angular.module('myApp', ['ngRoute']);
app.factory("services", ['$http', function($http) {
  var serviceBase = 'services/'
    var obj = {};
    obj.getTarefas = function(){
        return $http.get(serviceBase + 'tarefas');
    }
    obj.getTarefa = function(idTarefa){
        return $http.get(serviceBase + 'tarefa?idTarefa=' + idTarefa);
    }

    obj.insertTarefa = function (tarefa) {
    return $http.post(serviceBase + 'insertTarefa', tarefa).then(function (results) {
        return results;
    });
	 };

	obj.updateTarefa = function (idTarefa,tarefa) {
	    return $http.post(serviceBase + 'updateTarefa', {idTarefa:idTarefa, tarefa:tarefa}).then(function (status) {
	        return status.data;
	    });
	};

	obj.deleteTarefa = function (idTarefa) {
	    return $http.delete(serviceBase + 'deleteTarefa?idTarefa=' + idTarefa).then(function (status) {
	        return status.data;
	    });
	};

  obj.getNumeros = function(){
        return $http.get(serviceBase + 'numeros');
    }

    return obj;   
}]);

app.controller('principalCtrl', function ($scope, services) {
    
});

app.controller('numerosCtrl', function ($scope, services) {
    services.getNumeros().then(function(data){
        $scope.numeros = data;
    });
});

app.controller('listCtrl', function ($scope, services) {
    services.getTarefas().then(function(data){
        $scope.tarefas = data.data;
    });
});

app.controller('editCtrl', function ($scope, $rootScope, $location, $routeParams, services, tarefa) {
    var idTarefa = ($routeParams.idTarefa) ? parseInt($routeParams.idTarefa) : 0;
    $rootScope.title = (idTarefa > 0) ? 'Editar tarefa' : 'Adicionar tarefa';
    $scope.buttonText = (idTarefa > 0) ? 'Atualizar tarefa' : 'Adicionar tarefa';
    $scope.prioridades = [{"id":"ALTA", "nome":"ALTA"}, {"id":"MEDIA", "nome":"MEDIA"}, {"id":"BAIXA", "nome":"BAIXA"}];
      var original = tarefa.data;
      original.idTarefa = idTarefa;
      $scope.tarefa = angular.copy(original);
      $scope.tarefa.idTarefa = idTarefa;

      $scope.isClean = function() {
        return angular.equals(original, $scope.tarefa);
      }

      $scope.deleteTarefa = function(tarefa) {
        $location.path('/tarefas');
        if(confirm("Confirma exclusão da tarefa: "+$scope.tarefa.titulo)==true)
        services.deleteTarefa(tarefa.idTarefa);
      };

      $scope.salvarTarefa = function(tarefa) {
        $location.path('/tarefas');
        if (idTarefa <= 0) {
            services.insertTarefa(tarefa);
        }
        else {
            services.updateTarefa(idTarefa, tarefa);
        }
    };
});



app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        title: 'Principal',
        templateUrl: 'view/principal.html',
        controller: 'principalCtrl'
      }).
      when('/tarefas', {
        title: 'Tarefas',
        templateUrl: 'view/tarefas.html',
        controller: 'listCtrl'
      })
      .when('/editTarefa/:idTarefa', {
        title: 'Editar',
        templateUrl: 'view/editTarefa.html',
        controller: 'editCtrl',
        resolve: {
          tarefa: function(services, $route){
            var idTarefa = $route.current.params.idTarefa;
            return services.getTarefa(idTarefa);
          }
        }
      })
      .when('/numeros', {
        title: 'Números',
        templateUrl: 'view/numeros.html',
        controller: 'numerosCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
}]);
app.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
    });
}]);