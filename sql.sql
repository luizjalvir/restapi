CREATE DATABASE IF NOT EXISTS agenda;
 
USE agenda;


CREATE TABLE IF NOT EXISTS tarefa (
  codigo int(11) NOT NULL AUTO_INCREMENT,
  titulo varchar(100) NOT NULL,
  descricao varchar(255) NOT NULL,
  prioridade varchar(50) NOT NULL,
  PRIMARY KEY (codigo)
);